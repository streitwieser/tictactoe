#!/bin/bash
# From: https://community.atlassian.com/t5/Sourcetree-questions/SourceTree-external-diff-path-issue-on-Windows/qaq-p/394740
# Method to detrmine absolute path
# The -W parameter on the pwd command is necessary to return the Windows version of the path.
# Not using the -W parameter will result in a conversion of temp directory to a 'tmp' path meaningful only in the Linux environment.
# Piping the result through tr '/' '\\' translate the forward slashes to backslashes.
# Windows understands forward slashes, but LVCompare.exe does not.

# Please note:
# Move this script to the folder given in .gitconfig!

abspath () {
(
	DIR=$(dirname "$1")
	FN=$(basename "$1")
	cd "$DIR"
	printf "%s/%s" "$(pwd -W)" "$FN" | tr '/' '\\'
)
}

lvcompare="C:\\Program Files (x86)\National Instruments\\Shared\\LabVIEW Merge\\LVMerge.exe"
base=$(abspath "$1")
local=$(abspath "$1")
other=$(abspath "$2")
output=$(abspath "$2")

echo "Launching $lvmerge $base $other $local $output

exec "$lvmerge" "$base" "$other" "$local" "$output"

# For the options, see http://zone.ni.com/r...mp_thirdparty/.