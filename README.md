# README #

LabView program that plays TicTacToe.

## Please note ##

Add this to system, global or local .gitconfig file:

[diff "lvdiff"]
command = "C:/Users/Hans/AppData/Local/Programs/Git/bin/_LVCompareWrapper.sh" \"$REMOTE\" \"$LOCALE\"

[merge "lvmerge"]
name = "LabView 3-Way Merge"
driver = "C:/Users/Hans/AppData/Local/Programs/Git/bin/_LVMergeWrapper.sh" %O %A %B %L %P
recursive = binary