﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20006002">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="array_to_board.vi" Type="VI" URL="../array_to_board.vi"/>
		<Item Name="array_to_player.vi" Type="VI" URL="../array_to_player.vi"/>
		<Item Name="board_to_array.vi" Type="VI" URL="../board_to_array.vi"/>
		<Item Name="board_to_players.vi" Type="VI" URL="../board_to_players.vi"/>
		<Item Name="computer move.vi" Type="VI" URL="../computer move.vi"/>
		<Item Name="disable_control.vi" Type="VI" URL="../disable_control.vi"/>
		<Item Name="game over.vi" Type="VI" URL="../game over.vi"/>
		<Item Name="minimax.vi" Type="VI" URL="../minimax.vi"/>
		<Item Name="set_color.vi" Type="VI" URL="../set_color.vi"/>
		<Item Name="State.ctl" Type="VI" URL="../State.ctl"/>
		<Item Name="tictactoe.vi" Type="VI" URL="../tictactoe.vi"/>
		<Item Name="win_situation.vi" Type="VI" URL="../win_situation.vi"/>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
